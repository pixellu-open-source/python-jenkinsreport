__author__ = 'Alex Prykhodko'
__version__ = "0.1.3"

import argparse
from urllib import *
import json
import prettytable
import copy
import datetime
import os.path

CHANGELOG_FILE_NAME = "CHANGELOG"
UNI_TEST_RESULTS_FILE_NAME = "TESTSREPORT"


def parse_arguments():
    """
    Parses CLI arguments, and returns the parsed values enclosed in a Namespace object.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('url', help="URL of the remote Jenkins server.")
    parser.add_argument('job_name', help="Name of the job.")
    parser.add_argument('build_number', type=int, help="Build number to generate reports for.")
    parser.add_argument('-o', '--output-path', default='', help="Output path for generated reports. If none provided, "
                                                                "then the files will be written into "
                                                                "the working directory.")
    parser.add_argument('--exclude-change-log', action='store_true', help="If specified, then no change log file "
                                                                    "will be created.")
    parser.add_argument('--exclude-test-results', action='store_true', help="If specified, then no unit test results file "
                                                                      "will be created.")

    return parser.parse_args()


def generate_header(arguments):
    """
    Generates a header that will be included in report files.
    """

    return "Server URL: %s\nJob name: %s\nBuild number: %d\nReport date: %s\n" % (
        arguments.url,
        arguments.job_name,
        arguments.build_number,
        str(datetime.datetime.now())
    )


def generate_changeset_report(build_data):
    """
    Generates a PrettyTable object containing the changeset report.
    """
    assert 'changeSet' in build_data

    report_table = prettytable.PrettyTable(['Author', 'Comment'])
    report_table.align['Author'] = 'l'
    report_table.align['Comment'] = 'l'

    changeset = build_data['changeSet']
    for item in changeset['items']:
        report_table.add_row([item['author']['fullName'], item['comment'].strip()])

    return report_table


def generate_test_results_report(test_results):
    """
    Generates and returns a tuple containing of two PrettyTable objects containing the unit test results report.
    """
    assert 'suites' in test_results

    # Initialize the tables:

    failed_report_table = prettytable.PrettyTable(['Test Name', 'Duration', 'Status', 'Age'])
    failed_report_table.align['Test Name'] = 'l'
    failed_report_table.align['Duration'] = 'c'
    failed_report_table.align['Status'] = 'c'
    failed_report_table.align['Age'] = 'c'

    passed_report_table = copy.deepcopy(failed_report_table)

    # Work through JSON data received from the server:

    for suite in test_results['suites']:
        for case in suite['cases']:
            row = ['%s.%s' % (case['className'], case['name']), str(case['duration'] * 1000) + 'ms', case['status'], case['age']]
            if case['status'] == 'FAILED':
                failed_report_table.add_row(row)
            elif case['status'] == 'PASSED':
                passed_report_table.add_row(row)

    return failed_report_table, passed_report_table


def get_jenkins_data(jenkins_url, job_name, build_number):
    """
    Retrieves information about the build from the remote Jenkins server.
    There is no authentication support.
    """

    urls = {
        'build_info': '%s/job/%s/%d/api/json' % (jenkins_url, pathname2url(job_name), build_number),
        'test_results': '%s/job/%s/%d/testReport/api/json' % (jenkins_url, pathname2url(job_name), build_number)
    }

    result = {}

    for (key, url) in urls.iteritems():
        connection = urlopen(url)
        if connection.code == 200:
            result[key] = json.loads(connection.read())
        else:
            if connection.code == 404:
                raise Exception("Jenkins job or build were not found.")
            else:
                raise Exception("Unexpected Jenkins HTTP code received.")
        connection.close()

    return result


def main():

    arguments = parse_arguments()

    header = generate_header(arguments)

    jenkins_data = get_jenkins_data(arguments.url, arguments.job_name, arguments.build_number)
    changeset_report = generate_changeset_report(jenkins_data['build_info'])

    (failed_report_table, passed_report_table) = generate_test_results_report(jenkins_data['test_results'])

    # Write the results to files:

    if not arguments.exclude_change_log:
        with open(os.path.join(os.path.expanduser(arguments.output_path), CHANGELOG_FILE_NAME), 'w') as fp:
            fp.write(header + "\n")
            fp.write(str(changeset_report))
            fp.write("\n")

    if not arguments.exclude_test_results:
        with open(os.path.join(os.path.expanduser(arguments.output_path), UNI_TEST_RESULTS_FILE_NAME), 'w') as fp:
            fp.write(header + "\n")
            fp.write("FAILED TESTS:\n")
            fp.write(str(failed_report_table))
            fp.write("\n\n")
            fp.write("PASSED TESTS:\n")
            fp.write(str(passed_report_table))
            fp.write("\n")


if __name__ == "__main__":
    main()
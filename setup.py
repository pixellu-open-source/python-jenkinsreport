from distutils.core import setup
import jenkinsreport

setup(
    name='jenkinsreport',
    description='A CLI utility that generates simple change log and unit test results files by retrieving builds '
                'data from jobs on a remote Jenkins server (no authentication support).',
    version=jenkinsreport.__version__,
    author='Alex Prykhodko',
    author_email='alex@prykhodko.net',
    py_modules=['jenkinsreport'],
    requires=['prettytable', 'argparse'],
    entry_points={
        'console_scripts': [
            'jenkinsreport = jenkinsreport:main'
        ]
    }

)


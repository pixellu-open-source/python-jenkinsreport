Introduction
============

Jenkinsreport is a small CLI utility that generates simple change log and unit test results files by retrieving
builds data from jobs on a remote Jenkins server (no authentication support).

Usage
-----

    $ jenkinsreport <url> <job_name> <build_number>
    
Optional Arguments
------------------    
    -o OUTPUT_PATH, --output-path OUTPUT_PATH
                        Output path for generated reports. If none provided,
                        then the files will be written into the working
                        directory.
                        
    --no-change-log       If specified, then no change log file will be created.
    
    --no-test-results     If specified, then no unit test results file will be
                        created.

Example
-------

    $ jenkinsreport "http://zeus.pixellu.local:8080" "SmartAlbums Internal Build" 709


Output Preview
--------------

**Change log file:**

    Server URL: http://zeus.pixellu.local:8080
    Job name: SmartAlbums Internal Build
    Build number: 709
    Report date: 2014-08-30 18:29:20.057911
    
    +----------------+-------------------------------------------------------------------------------------------------------------------+
    | Author         | Comment                                                                                                           |
    +----------------+-------------------------------------------------------------------------------------------------------------------+
    | John Doe       | TS-1323, TS-1334 Fixed issue with JSON validation.                                                                |
    | Jane Doe       | TS-1330 Fixed reading color from EXIF information for Adobe Bridge.                                               |
    | John Doe       | TS-1322 Display alert after reloading images.                                                                     |
    | John Doe       | TS-1322 Detect if an image has a color profile via CGImage.                                                       |
    | John Doe       | TS-1322 Now display color profile alert after copy/paste actions.                                                 |
    | John Doe       | TS-1322 Removed unused code.                                                                                      |
    | John Doe       | TS-1348 Fixed bug with incorrect image resize in CV.                                                              |
    | Jane Doe       | TS-1323 Fixed problem with malformed JSON data in ACSF.                                                           |
    +----------------+-------------------------------------------------------------------------------------------------------------------+


**Unit test results file:**

    Server URL: http://zeus.pixellu.local:8080
    Job name: SmartAlbums Internal Build
    Build number: 709
    Report date: 2014-08-30 18:29:20.057911
    
    FAILED TESTS:
    +-----------------------------------------------------------------------+----------+--------+-----+
    | Test Name                                                             | Duration | Status | Age |
    +-----------------------------------------------------------------------+----------+--------+-----+
    | SmartAlbumsDataClassesUnitTest.testValidatingPathsAfterImportingPaths |  3.0ms   | FAILED |  7  |
    +-----------------------------------------------------------------------+----------+--------+-----+
    
    PASSED TESTS:
    +----------------------------------------------------------------------+----------+--------+-----+
    | Test Name                                                            | Duration | Status | Age |
    +----------------------------------------------------------------------+----------+--------+-----+
    | SmartAlbumsDataClassesUnitTest.testCreatingResources                 | 3916.0ms | PASSED |  0  |
    | SmartAlbumsDataClassesUnitTest.testDetectingLowResolutionImages      | 6446.0ms | PASSED |  0  |
    | SmartAlbumsDataClassesUnitTest.testFaceDetectionForImages            | 4056.0ms | PASSED |  0  |
    | SmartAlbumsDataClassesUnitTest.testImportingImages                   |  2.0ms   | PASSED |  0  |
    | SmartAlbumsDataClassesUnitTest.testImportingProcessWithImportedPaths |  6.0ms   | PASSED |  0  |
    +----------------------------------------------------------------------+----------+--------+-----+
